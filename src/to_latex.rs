//! AST to latex translation.

use std::io::Write ;
use std::io::Result as IoRes ;

use lib_texdown::ast::{ SmplTxt, RichTxt, Frame, Frames } ;

/// To latex translation.
pub trait ToLatex {
  /// Translates something to latex, writes it in the writer.
  fn to_latex<Writer: Write>(& self, & mut Writer) -> IoRes<()> ;
}

impl ToLatex for SmplTxt {
  #[allow(unused_imports)]
  fn to_latex<Writer: Write>(& self, w: & mut Writer) -> IoRes<()> {
    // Bring all variants of `SmplTxt` in scope.
    use lib_texdown::ast::SmplTxt::* ;

    match * self {
      ref txt => {
        println!("Warning: don't know how to print this simple txt:") ;
        println!("  > {:?}", txt) ;
        println!("  writing in debug mode for now.") ;
        write!(w, "{:?}", self)
      },
    }
  }
}

/// To latex translation, with indentation.
pub trait ToLatexIndent {
  /// Translates something to latex, writes it in the writer.
  fn to_latex<Writer: Write>(& self, & mut Writer, & str) -> IoRes<()> ;
}

impl ToLatexIndent for RichTxt {
  #[allow(unused_imports)]
  fn to_latex<Writer: Write>(
    & self, w: & mut Writer, indent: & str
  ) -> IoRes<()> {
    // Bring all variants of `RichTxt` in scope.
    use lib_texdown::ast::RichTxt::* ;

    match * self {
      ref txt => {
        println!("Warning: don't know how to print this rich txt:") ;
        println!("  > {:?}", txt) ;
        println!("  writing in debug mode for now.") ;
        write!(w, "{}{:?}\n", indent, self)
      },
    }
  }
}


// Implementing `ToLatexIndent` for `Frame` too.
impl ToLatex for Frame {
  fn to_latex<Writer: Write>(& self, w: & mut Writer) -> IoRes<()> {
    // Write start of frame, with indentation.
    try!( write!(w, "\\begin{{frame}}{{") ) ;
    // Write title (simple text).
    try!( self.title().to_latex(w) ) ;
    // Close title delimiter.
    try!( write!(w, "}}\n\n") ) ;

    // Write the body of the frame, indented by two spaces.
    try!( self.body().to_latex(w, "  ") ) ;

    // Write end of frame.
    write!(w, "\n\\end{{frame}}\n\n\n")
  }
}

// Implementing `ToLatexIndent` for `Frames` too.
impl ToLatex for Frames {
  fn to_latex<Writer: Write>(& self, w: & mut Writer) -> IoRes<()> {

    // Write header, see below for the definition of this function.
    try!( write_header(w) ) ;

    // Start document.
    try!( write!(w, "\\begin{{document}}\n\n") ) ;

    // Write all frames.
    for frame in self.get() {
      try!( frame.to_latex(w) )
    }

    // End document.
    write!(w, "\\end{{document}}\n\n")
  }
}





/// Allows to write something in latex to a file directly.
///
/// Builds on the `to_latex` function provided by `ToLatex`.
pub trait ToLatexFile: ToLatex {
  /// Writes something in latex to a file directly.
  fn to_latex_file(& self, target: & str) -> IoRes<()> {
    use std::fs::OpenOptions ;

    let mut file = try!(
      OpenOptions::new().create(true).truncate(
        true
      ).write(true).open(target)
    ) ;
    self.to_latex(& mut file)
  }
}

impl ToLatexFile for Frames {}





/// Writes the latex header for a presentation.
///
/// TODO: Add title and authors (and date?).
fn write_header<Writer: Write>(
  w: & mut Writer
) -> IoRes<()> {
  write!(w,
    r#"
\documentclass[10pt]{{beamer}}
\usetheme{{Warsaw}}

% |===| Package imports.

\usepackage{{
  etex, graphicx, amssymb, amsmath, amstext, amsfonts, mathtools,
  multicol, multirow, pgfplots, array, listings, colortbl, ulem, ifthen,
  xcolor, mathrsfs, xspace, rotating, soul
}}
\usepackage[scaled=1]{{beramono}}
\usepackage[T1]{{fontenc}}
\usepackage[utf8]{{inputenc}}
\usepackage{{tikz}}
\usepackage{{zlmtt}}

\usetikzlibrary{{shapes,arrows}}

\renewcommand{{\baselinestretch}}{{1.2}}

\hypersetup{{
  colorlinks=true,
  linkcolor=url,
  urlcolor=url
}}


"#
  )
}
